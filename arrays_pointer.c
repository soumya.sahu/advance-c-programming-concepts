#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

int product (int *array, int elements);

void findCharacters(char *p);

void array_memoryDisplay(int *arrayMem);

void stringPallindrome(char *string);

int main()
{
    int n =5;
    int A[] = {2,6,2,2,2};
    char *parrayMem = 'hello world';
    //printf("The product of %d elements: %d\n",n,product(A,n) );
    char *p = "MadaM";
    //findCharacters(p);
    //array_memoryDisplay(A);
    stringPallindrome(p);



    return 0;
}

int product (int *array, int elements){
    int product = 1;
    int *const end_element = array + elements;
    for(; array < end_element; array++){
        product *= *array;
    }
    return product;
}

void findCharacters(char *p){
    int count =0;
    while(*p) {
        if(*p == 'o') {
            ++count;
        }
        *p++;
    }
    printf("count : %d ",count);
}

void array_memoryDisplay(int *p){
    int count =0;
    while(*p){
        printf("arrayMem[%d] = %d | *(p+%d) = %d | &arrayMem[%d] = %p \n"
               ,count,*p, count, *(p), count, p);
        count++;
        *p++;
    }
}


void stringPallindrome(char *string){
    int length = strlen(string);
    char * n = string + (length-1);
    while(string < n){
        if(*string != *n){
            printf("String is not pallindrome\n");
            return 0;
        }
        ++string;
            --n;
    }
    printf("String is pallindrome\n");
}
