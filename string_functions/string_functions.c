#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* String functions :
strcpy()
strcat()
strlen()
strcmp()
strchr()
strstr()
strtok()
*/

//#define BASICS

void compare_string(char *sentence1, char *sentence2);
void findchar_string(char *sentence);
void findstring_string(char *sentence);
void tokenize_string(char *sentence);


int main()
{
    #ifdef BASICS
    char string_input[10];
    printf("Hello \0 world!\n"); //any word or character after "\0" will be neglected
    scanf("%s",string_input);
    printf("%s", string_input); //any word or character after space will be neglected
    #endif
    char *s1 = "Happy life";
    char *s2 = "Alive is awesome";
    char token_string[80] = "Happy :- Birthday to you-: GBU";
    printf("The length of s1 : %d | s2 : %d\n", strlen(s1),strlen(s2));
    //compare_string(s1,s2);
    //findchar_string(s2);
    //findstring_string(s2);
    tokenize_string(token_string);


    return 0;
}


void compare_string(char *sentence1, char *sentence2){
    printf("Is %s == %s?  %d \n",sentence1,sentence2,strcmp(sentence1,sentence2));
    printf("Is %s == %s?  %d \n",sentence1,sentence2,strcmp(sentence2,sentence1));
    char s3[50];
    strcpy(s3,sentence1);
    printf("Is %s == %s?  %d \n",sentence1,s3,strcmp(sentence1,s3));
    printf("The sentence is %s\n", strcat(s3,sentence2));
}

void findchar_string(char *sentence){
    char Ch = 'A';

    char *pCh = NULL;
    pCh = strchr(sentence,Ch);

    if(*pCh)
        printf("The address of %c is at %p and it is found at %s", Ch, pCh, pCh);

    else
        printf("Character not found");

}

void findstring_string(char *sentence){

    char Ch[] = "ve";

    char *pCh = NULL;
    pCh = strstr(sentence,Ch);

    if(*pCh)
        printf("The address of %s is at %p and it is found at %s", Ch, pCh, pCh);

    else
        printf("String not found");
}

void tokenize_string(char *sentence){

    char Ch[2] = "-:";
    char *token;

    token = strtok(sentence,Ch);

    while(token!= NULL){
        printf("%s\n",token);

        token = strtok(NULL,Ch);
    }


}

